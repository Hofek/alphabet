﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace KoreanLetters
{
    class Program
    {
        static int pass = 0;
        static int failed = 0;

        static List<string> letters = new List<string>();
        static List<string> transcript = new List<string>();
        static List<float> times = new List<float>();
        static List<string> answers = new List<string>();

        static void read_file()
        {
            string[] temp;
            using (StreamReader sr = new StreamReader("korean_letters.txt"))
            {           
                while(sr.Peek() >= 0)
                {
                    temp = sr.ReadLine().Split(' ');
                    letters.Add(temp[0]);
                    transcript.Add(temp[1]);
                    times.Add(0);
                    answers.Add("");
                }   
            }
        }

        static void show_config()
        {
            for (int a=0; a<letters.Count; ++a)
            {
                Console.WriteLine(letters[a] + " " + transcript[a]);
            }
        }

        static string get_Letter(List<string> l)
        {
            Random rnd = new Random();
            int r = rnd.Next(l.Count);
            return l[r];
        }

        static void game()
        {
            List<string> localLetters = new List<string>(letters);
            List<string> localTranscript = new List<string>(transcript);

            Stopwatch timer = new Stopwatch();

            while (localLetters.Count > 0)
            {
                Console.Clear();
                timer.Reset();

                Console.WriteLine("Runda: " + (pass + failed+1) + "/" + letters.Count);
                string letter = get_Letter(localLetters);
                Console.WriteLine(letter);
                timer.Start();
                string answer = Console.ReadLine();
                timer.Stop();

                answers[letters.IndexOf(letter)] = answer;

                if (localTranscript[localLetters.IndexOf(letter)] == answer)
                {
                    Console.WriteLine("Dobrze!");
                    ++pass;
                }
                else
                {
                    Console.WriteLine("Źle! Poprawna transkrypcja: " + localTranscript[localLetters.IndexOf(letter)]);
                    ++failed;
                }

                Console.WriteLine();
                times[letters.IndexOf(letter)] = timer.ElapsedMilliseconds / 1000;
                Console.WriteLine("Czas odpowiedzi: " + timer.ElapsedMilliseconds / 1000 + "s");

                int index = localLetters.IndexOf(letter);
                localLetters.RemoveAt(index);
                localTranscript.RemoveAt(index);

                Console.ReadKey();
            }


            
        }
        
        static void showresults()
        {
            Console.Clear();
            Console.WriteLine("Wynik " + pass + "/" + letters.Count);
            Console.WriteLine("=============");
            for (int a=0; a<letters.Count; ++a)
            {
                if (answers[a] == transcript[a]) Console.WriteLine(letters[a] + " " + answers[a] + " " + times[a] + " OK");
                else Console.WriteLine(letters[a] + " " + answers[a] + " " + times[a] + " FAIL (" + transcript[a] + ")");
            }
        }

        static void clear()
        {
            letters.Clear();
            transcript.Clear();
            answers.Clear();
            times.Clear();
            pass = 0;
            failed = 0;

            read_file();
        }

        static void Main(string[] args)
        {
            string choose = "t";
            Console.OutputEncoding = Encoding.UTF8;   

            while (choose[0] != 'n')
            {
                clear();
                game();
                showresults();
                Console.WriteLine("Wprowadź n, aby wyjść lub dowolny guzik aby kontynuować...");
                choose = Console.ReadLine();
            }
        }
    }
}
